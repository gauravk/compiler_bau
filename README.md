Welcome!

This is a creation of Gaurav Kukreja. 

This repository contains the assignments for Compiler Construction Lab Course 
at Technical University of Munich, in Summer Semester 2013.

While this is completely open source, you are suggested not to cheat for your
course. The journey is what it is all about, the destination is just the 
end of the fun. Enjoy the journey, don't take a shortcut to your destination.

Peace!

Instructions

1. COMPILE

ant compile

2. RUN

ant run


