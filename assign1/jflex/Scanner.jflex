package Example;

import java.io.*;
import java_cup.runtime.*;
import java_cup.runtime.SymbolFactory;

%%
%public
%class Scanner
%unicode
%cup
%line
%column

%{
	public Scanner(java.io.InputStream r, SymbolFactory sf){
		this(r);
		this.sf=sf;
	}
	private SymbolFactory sf;

%}

%init{
	/* This part is copied into the main function of the class verbatim */

%init}

%eofval{
    return sf.newSymbol("EOF",sym.EOF);
%eofval}

	
	SemiColon=";"
	Digit=[0-9]
	Letter=[a-z]
	Name={Letter}{Letter}*
	Number={Digit}{Digit}*
	Lte="<="
	Plus="+"
	Minus="-"
	Mult="*"
	Divide="/"
	Power="^"
	Equal="="
	Whitespace=" "|"\n"

%state OUTPUT_INIT
%state INPUT_INIT
%state POLYNOMIAL

%%

/* keywords */

	{Whitespace}			{		/* ignore */
						}
<YYINITIAL> {
	"OUTPUT"			{ 		
							yybegin(OUTPUT_INIT);
							return sf.newSymbol("OUTPUT", sym.OUTPUT);  	
						}
	"INPUT"				{ 		
							yybegin(INPUT_INIT);
							return sf.newSymbol("INPUT", sym.INPUT);  	
						}
	{Name}				{
							yybegin(POLYNOMIAL);
							return sf.newSymbol("Name", sym.NAME, yytext());
						}					
}

<OUTPUT_INIT> {
	{Whitespace}		{
							/* ignore */
						}
	{Name}				{ 	
							return sf.newSymbol("Name", sym.NAME, yytext());
						}
	{SemiColon}			{		
							yybegin(YYINITIAL);
							return sf.newSymbol("Symbol", sym.SEMICOLON);
						}
}

<INPUT_INIT> {
	{Whitespace}		{
							/* ignore */
						}
	{Number}			{
							return sf.newSymbol("Number", sym.NUMBER, yytext());
						}
	{Lte}				{ 	
							return sf.newSymbol("Lte", sym.LTE);
						}
	{Name}				{
							return sf.newSymbol("Name", sym.NAME, yytext());
						}
	{SemiColon}			{
							yybegin(YYINITIAL);
							return sf.newSymbol("Symbol", sym.SEMICOLON);
						}
}

<POLYNOMIAL> {
	{Whitespace}		{
							/* ignore */
						}
	{Name}				{
							return sf.newSymbol("Name", sym.NAME, yytext());
						}
	{Number}			{
							return sf.newSymbol("Number", sym.NUMBER, yytext());
						}
	{SemiColon}			{
							yybegin(YYINITIAL);
							return sf.newSymbol("Semicolon", sym.SEMICOLON);
						}
	{Equal}				{
							return sf.newSymbol("Equal", sym.EQUAL);
						}
	{Power}				{
							return sf.newSymbol("Power", sym.POWER);
						}
	{Divide}			{
							return sf.newSymbol("Divide", sym.DIVIDE);
						}
	{Mult}				{
							return sf.newSymbol("Mult", sym.MULT);
						}
	{Plus}				{
							return sf.newSymbol("Plus", sym.PLUS);
						}
	{Minus}				{
							return sf.newSymbol("Minus", sym.MINUS);
						}

}
